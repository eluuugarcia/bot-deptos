# Bot Deptos Scraper

[Clasificados La Voz](https://clasificados.lavoz.com.ar/inmuebles/departamentos?operacion=alquileres)

## Configuración

- Crear archivo .env en la carpeta principal con las siguientes constantes:

  ```
  SPREADSHEET_ID={ID del archivo de google sheets}
  TELEGRAM_TOKEN_BOT={TOKEN del bot de telegram que envía mensajes}
  TELEGRAM_USER_ID={TOKEN del usuario que recibe mensajes}
  ```

- Crear entorno virtual e instalar dependencias:
  - `virtualenv venv`
  - `source venv/bin/activate`
  - `pip3 install -r requirements.txt`

## Ejecución

- `python main.py`
