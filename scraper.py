import requests
from bs4 import BeautifulSoup
import json
import os
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
import telegram
from datetime import datetime
from dotenv import load_dotenv

load_dotenv()

# CONFIG TELEGRAM:
bot_token = os.getenv('TELEGRAM_TOKEN_BOT')
bot_chatID = os.getenv('TELEGRAM_USER_ID')
bot = telegram.Bot(bot_token)

SELECTING_COMMAND = 1

# CONFIG GOOGLE SHEETS
spreadsheet_id = os.getenv('SPREADSHEET_ID')
scopes = ['https://www.googleapis.com/auth/drive',
          'https://www.googleapis.com/auth/spreadsheets']

credentials = ServiceAccountCredentials.from_json_keyfile_name(
    os.getcwd()+'/client_secrets.json', scopes=scopes)
drive_service = build('drive', 'v3', credentials=credentials)
google_sheets_service = build('sheets', 'v4', credentials=credentials)


# CONFIG BÚSQUEDA:
barrios = ["Centro", "Alberdi", "Alto Alberdi", "General Paz", "Nueva Córdoba",
           "Guemes", "Observatorio", "Caseros", "Paso de los Andes", "Obrero", "Juniors"]

max_price_alquiler = 45000


def get_deptos_pendientes():
    range = "Todos!A:A"
    request = google_sheets_service.spreadsheets().values().get(
        spreadsheetId=spreadsheet_id, range=range, valueRenderOption="FORMULA")
    response = request.execute()
    matrix = response['values']
    return matrix


def isOldDepto(string, matrix):
    for a in matrix:
        if string in ''.join(a):
            return True
    return False


def add_depto(depto):
    row = [['=HIPERVINCULO("' + depto["link"] + '";"Link")'],
           [depto["title"]], [depto["price"]], [depto["expensas"]], [
               depto["barrio"]], [depto["cant_dormitorios"]],
           [depto["direccion"]], [depto["wpp"]], [depto["photos"][0]] if len(depto["photos"]) > 0 else [""]]

    resource = {
        "majorDimension": "COLUMNS",
        "values": row
    }
    range = "Todos!A:A"
    request = google_sheets_service.spreadsheets().values().append(
        spreadsheetId=spreadsheet_id,
        range=range,
        body=resource,
        valueInputOption="USER_ENTERED"
    )

    response = request.execute()


def getDeptoData(link, depto):
    html = requests.get(link)
    soup = BeautifulSoup(html.content, "html.parser")

    fecha = soup.find("div", class_="h5 center").text.replace(
        "Fecha de actualización: ", "").replace(".", "/")
    fecha = fecha[0:10]
    fecha = datetime.strptime(fecha, "%d/%m/%Y")
    now = datetime.now()
    expensas = soup.find("h3", class_="h4 mt0 main bolder")
    print(expensas)
    if expensas:
        expensas = expensas.text.replace(" Expensas ", "")

    diff = now - fecha
    if diff.days > 30:
        return None
    wpp = soup.find("div", class_="col col-12 px1 pb1")

    if wpp:
        try:
            wpp = wpp.find("form")["on"].replace(
                "submit:AMP.navigateTo(url='", "").split("&text")[0]
        except:
            wpp = ""

    if len(depto["photos"]) == 0:
        photos = soup.find_all(
            "amp-img", class_="contain")
        photos = [p['src'] for p in photos]
        if len(photos) > 0:
            depto["photos"] = photos
        else:
            depto["photos"] = [
                "https://imgcla.lavoz.com.ar/files/imagecache/landscape_1020_560_sc/imagefield_default_images/RB_noimagen_628x418.jpg"]

    direccion = soup.find("p", class_="h4 bolder m0")
    depto["direccion"] = direccion.text if direccion else depto["barrio"]
    depto["wpp"] = wpp
    depto["expensas"] = expensas

    return depto


def getDeptos(page, oldDeptos):
    print(f"Analizando página {page}")

    URL = f"https://clasificados.lavoz.com.ar/inmuebles/departamentos?precio-hasta={max_price_alquiler}&moneda=pesos&operacion=alquileres&provincia=cordoba&ciudad=cordoba&page={page}"

    headers = {'Content-Type': 'application/json'}

    html = requests.request("GET", URL, headers=headers)

    soup = BeautifulSoup(html.content, "html.parser")

    deptos_html = soup.find_all(
        "div", class_="col-6 flex flex-wrap content-start sm-col-3 md-col-3 align-top")

    for depto in deptos_html:
        link = depto.find("a", class_="text-decoration-none", target="_self")
        link = link["href"]

        if isOldDepto(link, oldDeptos):
            continue

        title = depto.find(
            "h2", class_="bold mx0 mt0 pt1 mb1 col-12 title-2lines h4")
        title = title.text
        price = depto.find("span", class_="price")
        if price and "consultar" in price.text:
            continue
        price = price.text

        price = price.replace("$ ", "").replace(".", "")
        barrio = depto.find(
            "div", class_="h5 mx0 mt0 mb1 col-12 font-light title-1lines")

        if barrio is None or barrio.text.strip() not in barrios:
            continue
        barrio = barrio.text.strip()

        cant_dormitorios = depto.find(
            "div", "gray mt0 mb2 col-12 flex items-center").text.strip()

        cant_dormitorios = cant_dormitorios.split("FichaInmueble_dormitorios")
        if cant_dormitorios and len(cant_dormitorios) > 1:
            cant_dormitorios = cant_dormitorios[1].split(" ")
            if cant_dormitorios and len(cant_dormitorios) > 0:
                cant_dormitorios = cant_dormitorios[1]
            else:
                cant_dormitorios = "?"
        else:
            cant_dormitorios = "?"

        if "Monoambiente" in cant_dormitorios:
            continue
        photos = depto.find_all("amp-img", class_="contain")

        photos = [p['src'] for p in photos]

        d = {}
        d["title"] = title
        d["price"] = price
        d["barrio"] = barrio
        d["cant_dormitorios"] = cant_dormitorios
        d["photos"] = photos
        d["link"] = link
        print(d["title"])
        d = getDeptoData(link, d)
        if d:
            add_depto(d)
            telegram_bot_sendtext(d)
    next_page = soup.find("a", class_="right button-narrow")
    if next_page is None:
        return
    getDeptos(page+1, oldDeptos)
    return


def telegram_bot_sendtext(depto):

    caption = f"* 📌 {depto['title']} *\n\n💲 {depto['price']}"

    print(depto["expensas"])

    if depto["expensas"]:
        caption = caption + f"\n\nExpensas: {depto['expensas']}"
    caption = caption + \
        f"\n\n🛏️ {depto['cant_dormitorios']} dormitorio(s)\n\n📍 {depto['direccion']}, {depto['barrio']}"

    photo = None

    if len(depto["photos"]) > 0:
        photo = depto["photos"][0]

    payload = {
        "chat_id": bot_chatID,
        "parse_mode": "Markdown",
        "text": caption,
        "disable_web_page_preview": False,
        "reply_markup": {
            "inline_keyboard": [[{"text": "✅ GUARDAR", "callback_data": 'guardar'}, {"text": "❌ DESCARTAR", "callback_data": 'descartar'}], [{"text": "📞 Whatsapp", "url": depto['wpp']}, {"text": "📍 Maps", "url": f"https://www.google.com/maps/search/{depto['direccion']},+Cordoba/"}, {"text": "🔗 LINK", "url": depto['link']}, ]]}
    }

    uri = "sendMessage"

    if photo:
        payload["photo"] = photo
        payload["caption"] = caption
        payload["parse_mode"] = "Markdown"
        uri = "sendPhoto"

    url = 'https://api.telegram.org/bot' + bot_token + \
        f'/{uri}'

    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request(
        "POST", url, headers=headers, data=json.dumps(payload))
    # print(response.json())
